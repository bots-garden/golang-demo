FROM gitpod/workspace-full:latest

USER gitpod

# -----------------------
# Install GoLang
# -----------------------

ENV GOLANG_VERSION="1.20"
ENV GOLANG_OS="linux"
ENV GOLANG_ARCH="amd64"

ENV GOPATH=$HOME/go-packages
ENV GOROOT=$HOME/go
ENV PATH=$GOROOT/bin:$GOPATH/bin:$PATH
RUN curl -fsSL https://dl.google.com/go/go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz | tar xzs \
    && printf '%s\n' 'export GOPATH=/workspace/go' \
                      'export PATH=$GOPATH/bin:$PATH' > $HOME/.bashrc.d/300-go

# -----------------------
# Install TinyGo
# -----------------------
ENV TINYGO_VERSION="0.28.1"
ENV TINYGO_ARCH="amd64"
RUN wget https://github.com/tinygo-org/tinygo/releases/download/v${TINYGO_VERSION}/tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
RUN sudo dpkg -i tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
RUN rm tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb

